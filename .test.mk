FLAGS := --cpp-flag=-I/home/leclerc/.vfs_build/ext/Catch2/install/include
FLAGS += --cpp-flag=-I/opt/homebrew/Cellar/eigen/3.4.0_1/include/
FLAGS += --cpp-flag=-I/opt/homebrew/Cellar/boost/1.87.0/include
FLAGS += --cpp-flag=-I/opt/homebrew/Cellar/gmp/6.3.0/include/
FLAGS += --cpp-flag=-Iext/tl20/src/cpp/
FLAGS += --cpp-flag=-Isrc/cpp/

FLAGS += --cpp-flag=-std=c++20
FLAGS += --cpp-flag=-O2
FLAGS += --cpp-flag=-g3

# FLAGS += --cpp-flag=-I/usr/X11R6/include
# FLAGS += --lib-flag=-L/usr/X11R6/lib
# FLAGS += --lib-flag=-lpthread
# FLAGS += --lib-flag=-lX11
# FLAGS += --lib-flag=-lm

# vfs_build run ${FLAGS} tests/cpp/test_PowerDiagram.cpp

all:
	# mamba run -n vfs vfs_build run ${FLAGS} tests/cpp/test_Density.cpp
	mamba run -n vfs vfs_build run ${FLAGS} tests/cpp/test_Solver.cpp
	