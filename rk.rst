Dassault => IA "prouvée"
présentation des thèmes des équipes INRIA
  frugal learning
  xxx-supervised
  life-long learning 
  trustworthiness
  knowledge integration
  

Sur internet, on dit ce qui est intéressant, pas ce qui est évident
  Peu de logique explicite dans les textes sur internet
Outils externes => difficile de formaliser. Pb par exemple d'ordre dans lequel appeler les outils.
internalisation => on entraine avec un outil de logique. Pb avec random sampling: difficulty is not interesting...
  But faire un sampleur bien senti.

Generative AI for virtual twins
    Ex: point cloud to physical valid object (ex: une chaise avec 4 pieds)

Physics informed
  * recover dynamics using for instance derivatives. "PINS"

Differentiable operators 

Neural operators
  * map bnd init cond => solutions of PDE 
  * mesh free
    => DeepONet 
    => vs fourier neural operators

Solver
  * graph-NN pour les maillages

  
Pb du 1D: pour la construction du diagramme par étape, à chaque fois qu'on a une collision (d'une cellule avec un ensemble de cellules déjà assemblées), il faut faire un calcul où toutes les cellules de l'ensemble doivent intervenir. Ça devient du O(n²)

Est-ce qu'à la place de ça, on pourrait chercher à solidifier le newton.
  Par exemple, on pourrait donner des bornes pour les dérivées de la densité.
  
Proposition: on travaille sur des intervalles pour les potentiels.
  on cherche ensuite des bornes polynomiale, qui pourraient être définies par 1 seule matrice et 2 vecteurs pour les aires.

  Rq: pour le 1D, on pourrait travailler sur les deltas de potentiels. 

Autre proposition: on paramétrise par le contraste de densité, et cherche à aller vers un contraste de 1 le plus vite possible.
  => il va y avoir des discontinuités

Rq: quand on construit le diagramme en ajoutant des point, on pourrait donner des intervalles pour le positionnement relatif de cellules.
  Si on est certains qu'il y a intersection ou certains qu'il n'y a pas intersection, on peut faire retarder le calcul exact, et peut être faire ça jusqu'à la fin.
  Rq: c'est sans doute plus facile de traiter une succession de problèmes 1D, par exemple au pire en faisant 


Pour un cellule seule, le `p` qu'on cherche, c'est bien celui qui permet d'avoir le dirac au milieu de l'intervalle
  Pour ce cas (une cellule seule), on peut faire une courbe qui en fonction du `beg_p` va donner la position du dirac
  Si les calculs sont bon, la position des diracs est linéaire entre deux points.



Comment ça va se passer pour des aglomérats ?
  

La proposition, ça pourrait être de chercher `F' = 0` en testant progressivement les intervalles.
  Par exemple, avec 2 cellule, on cherche  
    [ C^-1( C( Z ) + 1 * a ) - x_0 ]^2 + [ C^-1( C( Z ) + 2 * a ) - x_1 ]^2 = [ C^-1( C( Z ) + 0 * a ) - x_0 ]^2 + [ C^-1( C( Z ) + 1 * a ) - x_1 ]^2 
  Lorsqu'on est sur des intervalles connus de densité, C^-1( C( Z ) + j * a ) = Z + a_j (a_j, c'est la taille de l'intervalle, et ça change d'un intervalle à l'autre)
     [ Z + a_1 - x_0 ]^2 + [ Z + a_2 - x_1 ]^2 = [ Z + a_0 - x_0 ]^2 + [ Z + a_1 - x_1 ]^2
    => les Z^2 disparaissent

  En gro, compte tenu d'un ensemble de dirac, il faut qu'on estime le `F'` au début et à la fin de l'intervalle
    Malheureusement, pour les gros aglomérat, il faudra récupérer les fins d'intervalles pour chaque dirac...

  Proposition alternative : pour chaque cellule, on regarde dans quel intervalle de densité se trouvent les bords de cellule. 
    On peut procéder de façon récursive: on commence par un intervalle unique.
    Ça permet de trouver une position pour tout l'agglomérat.
    S'il est possible de diviser l'intervalle, on assigne un début et une fin d'intervalle pour bord de cellule.

    L'idéal serait de trouver dans quelles moitiées aller... mais il ne semble pas que ça soit possible sans faire tous les tests.

    Rq: la dichotomie fonctionnait... on pourrait juste ajouter une test pour regarder si le 0 est dans l'intervalle.
      Le cas échéant, on fait un early return avec la solution exacte.

    On devrait aller assez vite si le nombre d'intervalles n'est pas très important par rapport au nombre de diracs.

La proposition, c'est de chercher pour un offset donné
  * la validité en `off_p`,
  * le `off_p` qu'il faudrait pour pour trouve `F' = 0`.

Prop: on fait un boucle où
  * on cherche les nouveaux paquets (on regarde uniquement dans ceux qui ont bougé)

Pb: il faut qu'on repère les voisins des GCs. Ça veut a priori dire qu'il faut avoir prev et next
  Prop: on met les prev dans les moved s'ils n'y sont pas déjà 
    Du coup, il ne sera nécessaire de regarder que si les next touchent.


La proposition, c'est de mettre dans "gc_to_test" tous les débuts potentiels de trucs qui touchent.
  -> du coup, lorsqu'on déplace des cellule, il faut mettre aussi le prev_gc s'il n'y est pas déjà

Prochaine étape :
* optimisation, par exemple parallélisation lors de la recherche des positions optimales
* prouver qu'on n'a qu'une seule solution
* refaire les tests avec des points à positionner
  => on a de toutes façons un problème avec les points éloignés en nD, qui se chevauchent en 1D.
  Une solution pourrait être de définir un "droit de chevauchement".
  Autre prop: on integre les angles plutôt que prendre des valeurs discrètes. Ça va coûter de plus en plus cher en fonction du nombre de points.

* faire des tests en partiel
  * on pourrait par exemple faire un exemple avec 2 disques
  
Pour le recalage, on cherche la meilleure transformation affine
  On pourrait chercher les coefficients
  Pour trouver le centre de rotation, on peut chercher le point le plus proche de toutes les hyper plans perpendiculaires aux directions


Pb : comment obtenir des points répartis sur une demi boule ?
  Prop: on fait de la quantization optimale en travaillant les un UV d'un maillage de demi sphère

Comment faire une initialisation avec des diracs supprimés ?
  * c'est a priori assez simple avec un ratio de 100%

Rq: on pourrait aussi travailler sur des agglomérats de diracs, par exemple en mettant
  * le même poids sur les agglomérats
  * ou par exemple une interpolation  

Comment décider de la largeur de convolution ?
  * prop: on chercher une matrice "raisonnablement" inversible.
    * Rq: si les inconnues qui posent problème sont dans des cellules de la bonne aire, on ne va pas disqualifier le système dans son ensemble
  * On cherche ensuite à diminuer la largeur de convolution
    * on pourrait tester avec une largeur nulle, en partant des poids obtenus par extrapolation par exemple avec un ordre 3
      * On pourrait à ce moment là tester si le système est solvable. On propose une largeur de convolution intermédiaire (en la grossissant à chaque fois) jusqu'à tomber sur un système "solvable"

On pourrait chercher à remplacer le principe de la convolution par une pénalisation sur les poids
  L'idée pourrait être d'ajouter une matrice correspondant à une densité homogène 
  C'est plus facile d'ajouter une densité homogène, mais vraisemblablement, l'information est transmise moins vite.

  Prop: on teste les deux propositions.

On pourrait chercher à faire une convolution sur les poids.

Je vais aller chercher des exemples de problèmes de densité homogène pour voir si on peut les résoudre avec une convolution sur les poids.
Ensuite, il faudra voir si on peut résoudre les problèmes de densité homogène avec une convolution sur les poids.
À discuter avec Marie et Vincent.