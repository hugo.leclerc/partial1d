import cppimport.import_hook
import usdot_bindings
import numpy as np

def ot_diracs_to_piecewise_constant( dirac_positions, dirac_mass_ratios, density_positions, density_values, ot_parms = None ):
    if ot_parms is None:
        ot_parms = usdot_bindings.OtParms()
    return usdot_bindings.ot_diracs_to_piecewise_constant( dirac_positions, dirac_mass_ratios, density_positions, density_values, ot_parms )

